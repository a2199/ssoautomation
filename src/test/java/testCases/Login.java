package testCases;

import org.testng.annotations.Test;

import actions.LoginAction;
import utilities.BaseClass;
import utilities.ExcelUtils;

public class Login extends BaseClass {
	
	@Test(priority=1)
	public static void LoginTest() {
		System.out.println("Going for login");
		LoginAction.Login(ExcelUtils.getCellData(ExcelUtils.getRow("Login"),ExcelUtils.getColumn("Username")),ExcelUtils.getCellData(ExcelUtils.getRow("Login"),ExcelUtils.getColumn("Password")));
		logOut();
		System.out.println("Going for logout");
	}

}
