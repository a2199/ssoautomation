package testCases;

import org.testng.annotations.Test;

import actions.HomeAction;
import actions.LoginAction;
import utilities.BaseClass;
import utilities.ExcelUtils;

public class Home extends BaseClass {
	
	@Test
	public static void homeNavigation() {
		LoginAction.Login(ExcelUtils.getCellData(ExcelUtils.getRow("Login"),ExcelUtils.getColumn("Username")),ExcelUtils.getCellData(ExcelUtils.getRow("Login"),ExcelUtils.getColumn("Password")));
		HomeAction.homePageNavigation();
		logOut();
	}

}
