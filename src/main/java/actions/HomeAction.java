package actions;

import org.testng.Assert;

import pageObjects.HomePage;
import utilities.BaseClass;
import utilities.TaskScreenshot;
import utilities.UsefulUtiities;

public class HomeAction extends BaseClass {
	
	public static void homePageNavigation() {
		extentTest = extentReport.createTest("Navigation to home page");
		try {
			UsefulUtiities.getAction(driver).moveToElement(HomePage.getHomeIcon()).build().perform();
			boolean flag = HomePage.getHome().isDisplayed();
			try {
				Assert.assertTrue(flag);
				extentTest.pass("Home page menu displayed successfully.",TaskScreenshot.getScreenshot(driver));
			}catch(Exception e) {
				extentTest.fail("No home page menu displayed",TaskScreenshot.getScreenshot(driver));
			}
		}catch(Exception e) {
			extentTest.fatal("No home page element found");
		}
		
		
	}

}
