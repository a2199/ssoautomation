package actions;

import org.testng.Assert;

import pageObjects.PageRepository;
import utilities.BaseClass;
import utilities.TaskScreenshot;

public class LoginAction extends BaseClass {
	
	public static void Login(String Username, String Password) {
		
		System.out.println("*********************** Inside Login Method *************************");
		extentTest = extentReport.createTest("Login to the application");
		String title = "";
		try {
			PageRepository.getUsername().clear();
			PageRepository.getUsername().sendKeys(Username);
			extentTest.info("Entered username : "+Username);
			PageRepository.getPassword().clear();
			PageRepository.getPassword().sendKeys(Password);
			extentTest.info("Entered password : "+Password);
			PageRepository.getLoginButton().click();
			try {
				title = PageRepository.getTitle().getText();
				Assert.assertEquals(title,"shubham gupta");
				extentTest.info("Navigated to the home page : "+title);
				extentTest.pass("Successful navigated to home page",TaskScreenshot.getScreenshot(driver));
			}catch(Exception e) {
				extentTest.info("Failed while navigating to the home page : "+title);
				extentTest.fail("UnSuccessful navigation to home page",TaskScreenshot.getScreenshot(driver));
			}
		}catch(Exception e) {
			extentTest.fatal("Login element not found!!!");
		}
		
	}
	
}
