package utilities;

import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelUtils extends BaseClass {
	
	public static XSSFCell cell;
	
	public static void setExcelSetup() throws InvalidFormatException, IOException {
		workBook = new XSSFWorkbook(new FileInputStream(excelFilePath));
		workSheet = workBook.getSheet("Sheet1");
	}
	
	public static String getCellData(int RowNum, int ColumnNum) {
		System.out.println(RowNum+"-----"+ColumnNum);
		cell = workSheet.getRow(RowNum).getCell(ColumnNum);
		System.out.println("......."+cell);
		return cell.getStringCellValue();
	}
	
	public static int rowNum() {
		int lastRow1 = workSheet.getLastRowNum();
		return lastRow1;
	}
	
	public static int columnNum() {
		int lastColumn1 = workSheet.getRow(0).getLastCellNum();;
		return lastColumn1;
	}
	
	public static int getRow(String TestCase) {
		int lastRow = ExcelUtils.rowNum();
		int currentRow=0;
		for(int i=0;i<=lastRow;i++) {
			if(ExcelUtils.getCellData(i, 0).contentEquals(TestCase)) {
				currentRow = i;
				//System.out.println("Row:"+currentRow);
			}
		}
		return currentRow;
	}
	
	public static int getColumn(String column) {
		int lastColumn = ExcelUtils.columnNum();
		System.out.println("Last Column:"+lastColumn);
		int currentColumn = 0;
		for(int j=0;j<lastColumn;j++) {
			if(ExcelUtils.getCellData(0, j).contentEquals(column)) {
				currentColumn = j;
				//System.out.println("Column:"+currentColumn);
			}
		}
		return currentColumn;
	}
	
	public static void closeExcel() throws IOException {
		workBook.close();
	}
	
}
