package utilities;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.opera.OperaDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.github.bonigarcia.wdm.WebDriverManager;
import pageObjects.PageRepository;


public class BaseClass {
	
	public static WebDriver driver;
	
	public static ExtentReports extentReport; 
	public static ExtentTest extentTest;
	public ExtentHtmlReporter htmlreporter;
	public FileInputStream fis;
	public static Properties prop;
	public static FileInputStream fip;
	public static XSSFWorkbook workBook;
	public static XSSFSheet workSheet;
	public static XSSFCell cell;
	
	static File file = new File("src");
	public static String srcAbsolutePath = file.getAbsolutePath();
	
	static File propFile = new File("Properties/config.properties");
	public static String propFilePath = propFile.getAbsolutePath();
	
	static File excelFile = new File(srcAbsolutePath+"/main/java/testData/TestData.XLSX");
	public static String excelFilePath = excelFile.getAbsolutePath();
	
	@BeforeSuite
	public void initialization() throws IOException, InvalidFormatException {
		
		System.out.println("*************"+srcAbsolutePath);
		/* Initialization of Reporting Element */
		htmlreporter = new ExtentHtmlReporter(srcAbsolutePath+"/Reports/extent.html");
		extentReport = new ExtentReports();
		extentReport.attachReporter(htmlreporter);
		
		/* Load the property file */
		prop = new Properties();
		prop.load(new FileInputStream(propFilePath));
		
		/* Excel Setup */
		System.out.println(excelFilePath);
		ExcelUtils.setExcelSetup();
		
	}
	
	@BeforeTest
	public void launchBrowser() {
		if(getProperty("browser").equals("chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Long.parseLong(getProperty("Time")),TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(Long.parseLong(getProperty("Time")), TimeUnit.SECONDS);
			driver.get(getProperty("URL"));
		}else if(getProperty("browser").equals("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Long.parseLong(getProperty("Time")),TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(Long.parseLong(getProperty("Time")), TimeUnit.SECONDS);
			driver.get(getProperty("URL"));
		}else if(getProperty("browser").equals("Opera")) {
			WebDriverManager.operadriver().setup();
			driver = new OperaDriver();
			driver.manage().deleteAllCookies();
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Long.parseLong(getProperty("Time")),TimeUnit.SECONDS);
			driver.manage().timeouts().pageLoadTimeout(Long.parseLong(getProperty("Time")), TimeUnit.SECONDS);
			driver.get(getProperty("URL"));
		}else {
			System.out.println("Wrong Browser Launched");
		}
		
	}
	
	public static void logOut() {
		
		try{
			Thread.sleep(2000);
			PageRepository.settingIcon().click();
			Thread.sleep(5000);
			PageRepository.logout().click();
			Thread.sleep(5000);
		}catch(Exception e) {
			System.out.println("Logout Problem");
		}
		
	}
	
	/*@Test(priority=1)
	public void setUp1() throws IOException {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("https://www.google.com");
		extentTest = extentReport.createTest("Takes SCreenshot");
		extentTest.pass("Finally Captured",TaskScreenshot.getScreenshot(driver));
	}*/
	
	
/*	@Test(priority=2)
	public void things() {
		System.out.println("TestNG");
		extentTest = extentReport.createTest("Going to check the TestNG setup");
		extentTest.info("Successfully Done");
		extentTest.info("Launched Browser :"+getProperty("browser"));
	}*/
	
/*	@Test(priority=1)
	public void excel() {
		System.out.println("HUM"+ExcelUtils.getRow("Login"));
		System.out.println("HUM1"+ExcelUtils.getColumn("Username"));
		//System.out.println("--"+ExcelUtils.getRow("Login")+".Testing."+ExcelUtils.getColumn("Password"));
		//System.out.println("Going to read excel data : "+ExcelUtils.getCellData(ExcelUtils.getRow("Login"),ExcelUtils.getColumn("Username")));
	}
	*/
	@AfterSuite
	public void tearDown() throws IOException {
		driver.close();
		ExcelUtils.closeExcel();
		extentReport.flush();
	}
	
	public static String getProperty(String key) {
		return (String)prop.get(key);
	}
	
	
	
	
	
}
