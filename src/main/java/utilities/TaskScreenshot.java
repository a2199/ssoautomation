package utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;

public class TaskScreenshot extends BaseClass {
	
	public static TakesScreenshot takesScreenshot;
	public static File destFile;
	
	static File excelFile = new File(srcAbsolutePath+"/Screenshots");
	public static String excelFilePath = excelFile.getAbsolutePath();
	
	public static String captureScreenshot(WebDriver driver) throws IOException {
		takesScreenshot = (TakesScreenshot)driver;
		File srcFile = takesScreenshot.getScreenshotAs(OutputType.FILE);
		String destFilePath = "";
		 if(getProperty("Source").equals("local")){
			System.out.println("*********local********");
			System.out.println("*****************"+excelFilePath);
			destFile = new File(excelFilePath+"/"+System.currentTimeMillis()+".png");
			destFilePath = destFile.getAbsolutePath();
		 }else if(getProperty("Source").equals("Jenkins")){
			 System.out.println("*********Jenkins********");
			//String jenkinsPath = getProperty("ScreenshotPath");
			 destFile = new File(getProperty("ScreenshotPath")+"/"+System.currentTimeMillis()+".png");
			//destFilePath = jenkinsPath+"/"+System.currentTimeMillis()+".png";
			 destFilePath = destFile.getAbsolutePath();
		}
		System.out.println("*****************"+destFilePath);
		FileUtils.copyFile(srcFile, destFile);
		return destFilePath;
	}
	
	public static MediaEntityModelProvider getScreenshot(WebDriver driver) throws IOException {
		MediaEntityModelProvider memp = MediaEntityBuilder.createScreenCaptureFromPath(captureScreenshot(driver)).build();
		return memp;
	}

}
