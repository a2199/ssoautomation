package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utilities.BaseClass;

public class HomePage extends BaseClass {
	
	public static WebElement getHomeIcon() {
		WebElement homeIcon = driver.findElement(By.xpath("//i[@class='home icon']"));
		return homeIcon;
	}
	
	public static WebElement getHome() {
		WebElement homeIcon = driver.findElement(By.xpath("//span[contains(text(),'Home')]"));
		return homeIcon;
	}

}
