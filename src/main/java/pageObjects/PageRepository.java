package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import utilities.BaseClass;

public class PageRepository extends BaseClass {
	
	public static WebElement getUsername() {
		WebElement username = driver.findElement(By.name("email"));
		return username;
	}
	
	public static WebElement getPassword() {
		WebElement password = driver.findElement(By.name("password"));
		return password;
	}
	
	public static WebElement getLoginButton() {
		WebElement button = driver.findElement(By.xpath("//div[contains(text(),'Login')]"));
		return button;
	}
	
	public static WebElement getTitle() {
		WebElement title = driver.findElement(By.xpath("//span[contains(text(),'shubham gupta')]"));
		return title;
	}
	
	public static WebElement settingIcon() {
		WebElement setting = driver.findElement(By.xpath("//*[@class='settings icon']"));
		return setting;
	}
	
	public static WebElement logout() {
		WebElement logout = driver.findElement(By.xpath("//*[contains(text(),'Log Out')]"));
		return logout;
	}
	
}
